({
    getComponents : function(component,event,helper){
        
        component.set("v.showSpinner",true);
        
        var action = component.get("c.getMetadataComponentsAndLanguages");
        
        action.setCallback(this,function(response){
            
            var result = response.getReturnValue();
            
            if( response.getState() == 'SUCCESS' && result.Languages ){
                
                var langs = JSON.parse(result.Languages);
                var languages = [];
                
                for(var i = 0; i < langs.length; i++){
                    
                    var mLang = {};
                    
                    mLang.label=langs[i];
                    mLang.selected=false;
                    
                    languages.push(mLang);
                }
                
                component.set("v.languages",languages);
            }
            
            else{
                
                component.set("v.languages","None");
                
            }
            
            if( response.getState() == 'SUCCESS' && result.Components ){
                
                var components = JSON.parse(result.Components);
                
                component.set("v.components",components); 
                component.set("v.selectedComponent",components[0]);
                
                helper.gettingCategories(component,event,helper);
            }
            
            else{
                
                component.set("v.components","----None----"); 
                component.set("v.categories","----None----");
            }
            
            component.set("v.showSpinner",false);
            
        });
        
        $A.enqueueAction(action);
        
    },
    
    gettingCategories : function(component, event, helper){
        
        component.set("v.showSpinner",true);
        
        var selectedComponent = component.get("v.selectedComponent");
        
        var action = component.get("c.getCategories");
        
        action.setParams({
            "selectedComponent" : selectedComponent
        });
        
        action.setCallback(this, function(response){
            
            if( response.getState() == 'SUCCESS' ){
                var result = response.getReturnValue();
                
                if( result.Category ){
                    
                    component.set("v.categories",JSON.parse(result.Category));
                    component.set("v.showButton",true);
                    component.set("v.showTable",false);
                    component.set("v.selectedCategory",null);
                }
                
                
                else{
                    component.set("v.categories","----None----");
                    component.set("v.showButton",false);
                    component.set("v.showTable",false);
                }
                
            }
            
            component.set("v.showSpinner",false);
            
        });
        
        $A.enqueueAction(action);
        
    },
    
    categoryChange : function(component, event, helper,category){
        
        component.set("v.selectedCategory", category);  
        
        if( component.get("v.showButton") == false ){
            
            component.set("v.showSpinner",true);
            
            var selectedLanguage = component.get("v.selectedLanguages");
            var comp = component.get("v.selectedComponent");
            
            var action = component.get("c.getLabelAccordingToCategory");
            
            action.setParams({
                "selectedComponent" : comp,
                "category" : category,
                "selectedLanguages"	: selectedLanguage
            });
            
            action.setCallback(this, function(response){
                
                if( response.getState() == 'SUCCESS' ){
                    
                    var result = response.getReturnValue();
                    
                    component.set("v.recordList",result);
                    
                    component.set("v.totalPage",Math.ceil(result.length/component.get("v.size")));
                    //component.set("v.currentPage",1);
                    
                    helper.navigate( component );
                    
                }
                
                else{
                    
                    helper.showToast(component, event, helper);
                    
                }
                
                component.set("v.showTable",true);
                component.set("v.showSpinner",false);
                
            });
            
            $A.enqueueAction(action);
            
        }
    },
    
    languageChange : function(component, event,helper, selectedLanguage){
        
        component.set("v.showSpinner",true);
        
        var labels = component.get("v.recordList");
        
        var category = component.get("v.selectedCategory");
        var languages = component.get("v.language")
        
        var selectedComp = component.get("v.selectedComponent")
        var action = component.get("c.getLabelAccordingToCategory");
        
        action.setParams({
            "category" : category,
            "selectedLanguages" : selectedLanguage,
            "selectedComponent" : selectedComp
        });
        
        action.setCallback(this, function(response){
            
            if( response.getState() == 'SUCCESS' ){
                
                var result = response.getReturnValue();
                
                console.log('result'+result);
                
                component.set("v.totalPage",Math.ceil(result.length/component.get("v.size")));
                //component.set("v.currentPage",1);
                
                component.set("v.recordList",result);
                
                helper.navigate( component );
                
            }
            
            else{
                
                helper.showToast(component, event, helper);
                
            }
            
            component.set("v.showSpinner",false);

        });
        
        $A.enqueueAction(action);
        
    },
    
    createTranslation : function(component,event,helper){
        
        component.set("v.showSpinner",true);
        
        var action = component.get("c.createTranslation");
        
        var languages = component.get("v.selectedLanguages");
        var labelsData = component.get("v.recordList");
        
        action.setParams({
            "selectedLanguages" : languages,
            "labelJSON" : JSON.stringify(labelsData),
            "selectedComponent" : component.get("v.selectedComponent"),
            "category" : component.get("v.selectedCategory")
        });
        
        action.setCallback(this, function(response){
            
            if( response.getState() == 'SUCCESS' ){
                
                var result = response.getReturnValue();
                
                if(result == 'Success'){
                    
                    swal('Success !', 'Custom Label Translation Successfully Created', "success");
                    
                }
                
            }
            
            else{
                
                helper.showToast(component, event, helper);
            }
            
            component.set("v.showSpinner",false);
            
        });
        
        $A.enqueueAction(action);
    },
    
    setInfoText: function(component, labels) {
        
        if (labels.length === 0) {
            
            component.set("v.infoText", "Select Languages...");
            
        }
        
        if (labels.length === 1) {
            
            component.set("v.infoText", labels[0]);
            
        }
        
        else if (labels.length > 1) {
            
            component.set("v.infoText", labels.length+" languages selected");
            
        }
        
    },
    
    getSelectedLanguages: function(component,helper){
        
        var options = component.get("v.languages");
        
        var values = [];
        
        if(options!==undefined){
            
            options.forEach(function(element) {
                
                if (element.selected) {
                    
                    values.push(element.label);
                    
                }
                
            });
            
        }
        return values;
        
    },
    
    translateTheValues : function(component,event,helper){
        
        component.set("v.showSpinner",true);
        
        var labels = component.get("v.recordList");
        var languages = component.get('v.selectedLanguages');
        
        var action = component.get("c.translation");
        
        action.setParams({
            "languages" : languages,
            "labelJSON" : JSON.stringify(labels)
        });
        
        action.setCallback(this, function(response){
            
            if( response.getState() == 'SUCCESS' ){
                
                var result = response.getReturnValue();
                
                component.set('v.recordList',result);
                
                helper.navigate( component );
                
            }
            
            component.set("v.showSpinner",false);
            
        });
        
        $A.enqueueAction(action);
        
    },
    
    navigate : function(component){
        
        var records = [];
        var recordList = component.get("v.recordList");
        var currentPage = component.get("v.currentPage");
        var size = component.get("v.size");
        
        
        for( var i=(currentPage-1)*size;i<currentPage*size;i++ ){
            
            if(recordList[i])
                records.push( recordList[i] );
            
        }
        
        component.set( "v.records",records );
        
    },
    
    showToast : function(component, event, helper) {
        
        var toastEvent = $A.get("e.force:showToast");
        
        toastEvent.setParams({
            "title": "warning!",
            "message": "Please check that Translation Workbench is enable in your org."
        });
        
        toastEvent.fire();
    }
    
})