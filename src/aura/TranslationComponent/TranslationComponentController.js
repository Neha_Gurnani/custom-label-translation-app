({
    doInit : function(component, event, helper) {
        
        helper.getComponents(component,event,helper);
        
    },
    
    onComponentChange : function(component, event , helper){
        
        var comp = event.target.value;
        
        component.set("v.selectedComponent",comp);
        
        component.set("v.categories","");
        
        helper.gettingCategories(component, event, helper);
        
    },
    
    showData : function(component,event,helper){
        
        var category = component.get("v.categories");
        var selcectedCategory = component.get("v.selectedCategory");
        
        component.set("v.showButton",false);
        
        if( selcectedCategory ){
            
            helper.categoryChange(component, event, helper,selcectedCategory);
            
        }
        else{
            
            helper.categoryChange(component, event,helper, category[0]);
            
        }
        
    },
    
    onCategoryChange : function(component, event, helper){
        
        var category = event.target.value;
        helper.categoryChange( component, event, helper,category );
        
    },
    
    onSaveTranslation : function(component,event,helper){
        
        helper.createTranslation( component,event,helper );
        
    },
    
    handleClick: function(component, event, helper) {
        
        var mainDiv = component.find('main-div');
        $A.util.addClass(mainDiv, 'slds-is-open');
        
    },
    
    handleSelection: function(component, event, helper) {
        var showTable = component.get("v.showTable");
        
        console.log('showData ',showTable);
        
        var item = event.currentTarget;
        
        if (item && item.dataset) {
            
            var value = item.dataset.value;
            console.log('value '+value);
            var selected = item.dataset.selected;
            console.log('selected ',selected);
            var options = component.get("v.languages");
            
            if (event.ctrlKey) {
                
                options.forEach(function(element) {
                    
                    if (element.label === value) {
                        
                        element.selected = selected === "true" ? false : true;
                        console.log('if',element);
                        
                    }
                    
                });
                
            } else {
                
                options.forEach(function(element) {
                    
                    if (element.label === value) {
                        
                        element.selected = selected === "true" ? false : true;
                        
                    } else {
                        
                        element.selected = false;
                        
                    }
                    
                });
                
                var mainDiv = component.find('main-div');
                
                $A.util.removeClass(mainDiv, 'slds-is-open');
                
            }
            
            component.set("v.languages", options);
            
            var languages = helper.getSelectedLanguages(component,helper);
            
            component.set("v.selectedLanguages",languages);
            
            helper.setInfoText(component, languages);
            
            if( showTable ){
                
                helper.languageChange(component, event,helper,languages);
                
            }
        }
        
    },
    
    handleMouseLeave: function(component, event, helper) {
        
        var mainDiv = component.find('main-div');
        
        $A.util.removeClass(mainDiv, 'slds-is-open');
        
    },
    
    handleMouseEnter: function(component, event, helper) {
        
        component.set("v.dropdownOver", true);
        
    },
    
    handleMouseOutButton: function(component, event, helper) {
        
        window.setTimeout(
            
            $A.getCallback(function() {
                
                if (component.isValid()) {
                    
                    if (component.get("v.dropdownOver")) {
                        
                        return;
                        
                    }
                    
                    var mainDiv = component.find('main-div');
                    
                    $A.util.removeClass(mainDiv, 'slds-is-open');
                    
                }
                
            }), 200
            
        );
        
    },
    
    translateValues : function(component,event,helper){
        
        var selectedLanguage = component.get("v.selectedLanguages");
        
        if(selectedLanguage.length > 0){
            var btn = component.find("saveBtn");
            btn.set("v.disabled",false);
        }
        helper.translateTheValues(component,event,helper);
    },
    
    onNext : function ( component,event,helper ){
        
        var currentPage = component.get("v.currentPage");
        var totalPage = component.get("v.totalPage");
        
        if( currentPage < totalPage){
            component.set("v.currentPage",currentPage+1);
            helper.navigate( component );
        }
        
    },
    
    onPrevious : function( component,event,helper ){
        
        var currentPage = component.get("v.currentPage");
        var totalPage = component.get("v.totalPage");
        
        if( currentPage != 1 ){
            component.set("v.currentPage",currentPage-1);
            helper.navigate( component );
        }
        
    }
    
    
})