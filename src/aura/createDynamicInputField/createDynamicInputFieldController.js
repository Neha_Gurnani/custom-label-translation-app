({
	doInit : function(component, event, helper) {
      
        var record = component.get("v.record");
        var translation = record.langWithTranslation[component.get("v.language")];
        component.set("v.translation",translation);
       
    },
    onTranslationChange : function(component, event, helper){
        var val = event.getSource().get("v.value");
        var labelApi = event.getSource().get("v.name");
        
        var language = component.get("v.language");
        var records = component.get("v.records");
        
        for(var i =0 ; i<records.length;i++){
            if(records[i].labelAPIName == labelApi){
            records[i].langWithTranslation[language] = val;
            }
        }
    }
 })