/* 
 * Name        : TranslationComponentMock
 * Created By  : Concret.io Apps
 * Description : Class for Creating Mock data for Metadata API
*/

@isTest
global class TranslationComponentMock implements WebServiceMock{
    global void doInvoke(
        Object stub,
        Object request,
        Map<String, Object> response,
        String endpoint,
        String soapAction,
        String requestName,
        String responseNS,
        String responseName,
        String responseType
    ){
        
        List<MetadataService.FileProperties> files = new List<MetadataService.FileProperties>();
        
        System.debug('soapAction :: '+soapAction);
        System.debug('requestName :: '+requestName);
        System.debug('responseType :: '+responseType);
        
        if( requestName == 'listMetadata' ){
            MetadataService.FileProperties file = new MetadataService.FileProperties();
            
            file.fullName = 'Test';
            file.type_x = 'CustomLabel';
            files.add(file);
            
            MetadataService.listMetadataResponse_element response_x = new MetadataService.listMetadataResponse_element();
            response_x.result = files;
            response.put('response_x',response_x);
            
        }
        
        else if( requestName == 'readMetadata' ){
            
            if( responseType == 'MetadataService.readCustomLabelResponse_element' ){
                
                List<MetadataService.CustomLabel> customLabels = new List<MetadataService.CustomLabel>();
                
                MetadataService.CustomLabel customLabel = new MetadataService.CustomLabel();
                customLabel.fullName = 'Test';
                customLabel.value = 'TestingCustomLabel';
                
                customLabels.add( customLabel );
                
                MetadataService.ReadCustomLabelResult readCustomLabel = new MetadataService.ReadCustomLabelResult();
                readCustomLabel.records = customLabels;
                
                MetadataService.readCustomLabelResponse_element response_x = new MetadataService.readCustomLabelResponse_element();
                response_x.result = readCustomLabel;
                response.put('response_x',response_x);
                
            }
            
            else if( responseType == 'MetadataService.readTranslationsResponse_element' ){
                
                MetadataService.CustomLabelTranslation labelTranslation = new MetadataService.CustomLabelTranslation();
                labelTranslation.name = 'Test';
                labelTranslation.label = 'Tester';
                
                MetadataService.Translations translation = new MetadataService.Translations();
                translation.fullName = 'fr';
                translation.customLabels = new List<MetadataService.CustomLabelTranslation>{labelTranslation};
                    
                List<MetadataService.Translations> translations = new List<MetadataService.Translations>();
                translations.add( translation );
                
                MetadataService.ReadTranslationsResult readTranslation = new MetadataService.ReadTranslationsResult();
                readTranslation.records = translations;
                
                MetadataService.readTranslationsResponse_element response_x = new MetadataService.readTranslationsResponse_element();
                response_x.result = readTranslation;
                response.put('response_x',response_x);
                
            }
            
            else if( responseType == 'MetadataService.readCustomObjectResponse_element' ){
                
                MetadataService.FieldSet fieldSet = new MetadataService.FieldSet();
                fieldSet.fullName = 'Test_Field_Set_Of_Account';
                fieldSet.label = 'Test Field Set Of Account';
                
                MetadataService.CustomField customField = new MetadataService.CustomField();
                customField.fullName = 'Test_Custom_Field__c';
                customField.type_x = 'Text';
                customField.length = 25;
                customField.label = 'Test Custom Field';
                
                MetadataService.RecordType recordType = new MetadataService.RecordType();
                recordType.active = true;
                recordType.fullName = 'Test_Record_Type';
                
                MetadataService.WebLink webLink = new MetadataService.WebLink();
                webLink.fullName = 'Test_Link';
                webLink.masterLabel = 'Test Link';
                
                MetadataService.CustomObject customObject = new MetadataService.CustomObject();
                customObject.fieldSets = new List<MetadataService.FieldSet>{fieldSet};
                customObject.fields = new List<MetadataService.CustomField>{customField};
                customObject.recordTypes = new List<MetadataService.RecordType>{recordType};
                customObject.webLinks = new List<MetadataService.WebLink>{webLink};
                                
                MetadataService.ReadCustomObjectResult readCustomObject = new MetadataService.ReadCustomObjectResult();
                readCustomObject.records = new List<MetadataService.CustomObject>{customObject};
                    
                MetadataService.readCustomObjectResponse_element response_x = new MetadataService.readCustomObjectResponse_element();
                response_x.result = readCustomObject;
                response.put('response_x',response_x);  
                
            }
            
            else if( responseType == 'MetadataService.readWorkflowResponse_element' ){
                
                MetadataService.WorkflowTask task = new MetadataService.WorkflowTask();
                task.fullName = 'Test_Task';
                task.subject = 'Test Workflow Task on Account';
                
                
                MetadataService.Workflow workflow = new MetadataService.Workflow();
                workflow.fullName = 'Account';
                workflow.tasks	= new List<MetadataService.WorkflowTask>{task};	
                    
                MetadataService.ReadWorkflowResult workflowResult = new MetadataService.ReadWorkflowResult();
                workflowResult.records = new List<MetadataService.Workflow>{workflow};
                    
                 MetadataService.readWorkflowResponse_element response_x = new MetadataService.readWorkflowResponse_element();
                response_x.result = workflowResult;
                response.put('response_x',response_x); 
                
            }
            
            else if( responseType == 'MetadataService.readCustomObjectTranslationResponse_element' ){
                
                MetadataService.FieldSetTranslation fieldSetTranslation = new MetadataService.FieldSetTranslation();
                fieldSetTranslation.name = 'Test_Field_Set_Of_Account';
                fieldSetTranslation.label = 'Тестово поле Задайте профил';
                
                MetadataService.WorkflowTaskTranslation workflowTaskTranslation = new MetadataService.WorkflowTaskTranslation();
                workflowTaskTranslation.name = 'Test_Task';
                workflowTaskTranslation.subject = 'Задача Тест на работния поток по сметка';
                
                MetadataService.CustomFieldTranslation customFieldTranslation = new MetadataService.CustomFieldTranslation();
                customFieldTranslation.name = 'Test_Custom_Field__c';
                CustomFieldTranslation.label = 'Тествайте потребителското поле';
                
                MetadataService.WebLinkTranslation webLinkTranslation = new MetadataService.WebLinkTranslation();
                webLinkTranslation.name = 'Test_Link';
                webLinkTranslation.label = 'Тестова връзка';
                
                MetadataService.RecordTypeTranslation recordTypeTranslation = new MetadataService.RecordTypeTranslation();
                recordTypeTranslation.name = 'Test_Record_Type';
                recordTypeTranslation.label = 'Тип тест запис';
                
                MetadataService.CustomObjectTranslation customObjectTranslation = new MetadataService.CustomObjectTranslation();
                customObjectTranslation.fullName = 'Account-bg';
                customObjectTranslation.fieldSets = new List<MetadataService.FieldSetTranslation>{fieldSetTranslation};
                customObjectTranslation.workflowTasks = new List<MetadataService.WorkflowTaskTranslation>{workflowTaskTranslation};
                customObjectTranslation.fields = new List<MetadataService.CustomFieldTranslation>{customFieldTranslation};
                customObjectTranslation.webLinks = new List<MetadataService.WebLinkTranslation>{webLinkTranslation};
                customObjectTranslation.recordTypes = new List<MetadataService.RecordTypeTranslation>{recordTypeTranslation};
                                    
                MetadataService.ReadCustomObjectTranslationResult customObjectTranslationResult = new MetadataService.ReadCustomObjectTranslationResult();
                customObjectTranslationResult.records = new List<MetadataService.CustomObjectTranslation>{customObjectTranslation};
                    
                MetadataService.readCustomObjectTranslationResponse_element response_x = new MetadataService.readCustomObjectTranslationResponse_element();
                response_x.result = customObjectTranslationResult;
                response.put('response_x',response_x); 
                
            }
        }
        
        else if( requestName == 'createMetadata' ){
            
            if( responseType == 'MetadataService.createMetadataResponse_element' ){
                
                MetadataService.SaveResult result = new MetadataService.SaveResult();
                result.fullName = 'fr';
                result.success = true;
                
                List<MetadataService.SaveResult> saveResults = new List<MetadataService.SaveResult>();
                saveResults.add(result); 
                
                MetadataService.createMetadataResponse_element response_x = new MetadataService.createMetadataResponse_element();
                response_x.result = saveResults;
                response.put('response_x',response_x); 
                
            }
        }
    }
    
}