/* 
 * Name        : TranslationAPIMock
 * Created By  : Concret.io Apps
 * Description : Class for Creating Mock data for Translate API
*/

@isTest
global class TranslationAPIMock implements HttpCalloutMock{
    
    global HTTPResponse respond(HTTPRequest req) {
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"code": 200,"lang": "en-ja","text": ["カスタムフィールドをテスト"]}');
        res.setStatusCode(200);
        
        return res;
        
    }
    
    
}