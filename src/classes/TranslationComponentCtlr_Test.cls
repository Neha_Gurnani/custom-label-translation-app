/* 
 * Name        : TranslationComponentCtlr_Test
 * Created By  : Concret.io Apps
 * Description : Test class of the ObjectRecordsController controller
*/

@isTest
private class TranslationComponentCtlr_Test {
    
    @TestSetup
    private static void createCustomSettings(){
        
        List<Metadata_Component__c> components = new List<Metadata_Component__c>();
        
        components.add( new Metadata_Component__c( Name = 'Custom Label' ));
        components.add( new Metadata_Component__c( Name = 'Field Set' ));
        components.add( new Metadata_Component__c( Name = 'Record Type' ));
        components.add( new Metadata_Component__c( Name = 'Workflow Task' ));
        components.add( new Metadata_Component__c( Name = 'Custom Field' ));
        components.add( new Metadata_Component__c( Name = 'Button and Link Label' ));
        
        insert components;
        
        List<Languages__c> languages = new List<Languages__c>();
        
        languages.add(new Languages__c(Name='Bulgarian', Language_Code__c='bg', Locale_Code__c='bg' ));
        languages.add(new Languages__c(Name='Chinese', Language_Code__c='zh', Locale_Code__c='zh_TW' ));
        languages.add(new Languages__c(Name='French', Language_Code__c='fr', Locale_Code__c='fr' ));
        languages.add(new Languages__c(Name='German', Language_Code__c='de', Locale_Code__c='de' ));
        languages.add(new Languages__c(Name='Japanese', Language_Code__c='ja', Locale_Code__c='ja' ));
        
        insert languages;
    }
    
    @isTest
    private static void getMetadataComponentsAndLanguagesTest(){
        
        Test.startTest();
        
        Map<String,String> MetadataComAndLanguage = TranslationComponent_Ctlr.getMetadataComponentsAndLanguages();
        
        System.debug('MetadataComAndLanguage :: '+MetadataComAndLanguage);
        
        List<String> components = (List<String>)JSON.deserialize(MetadataComAndLanguage.get('Components'), List<String>.class);
        
        List<String> languages = (List<String>)JSON.deserialize(MetadataComAndLanguage.get('Languages'), List<String>.class);
        
        Test.stopTest();
        
        System.assertEquals( 6, components.size() );
        
        System.assertEquals( 5, languages.size() );
        
    }
    
    @isTest
    private static void getCategoriesForCustomLabel(){
        
        Test.startTest();
        
        Test.setMock( WebServiceMock.class, new TranslationComponentMock() );
        
        Map<String,String> categories = TranslationComponent_Ctlr.getCategories('Custom Label');
        
       	Test.stopTest();
        
        System.assertEquals( '["None"]', categories.get('Category') );
    }
    
    @isTest
    private static void getObjectsForFieldSet(){
        
        Test.startTest();
        
        Map<String,String> objects = TranslationComponent_Ctlr.getCategories('Field Set');
        
        Test.stopTest();
        
        System.assertEquals(true, !objects.isEmpty() );
        
    }
    
    
    @isTest
    private static void getRecordsForCustomLabelComponent(){
        
        Test.startTest();	
        
        Test.setMock( WebServiceMock.class, new TranslationComponentMock() );
        
        List<TranslationComponent_Ctlr.LabelWrapper> labelsList = TranslationComponent_Ctlr.getLabelAccordingToCategory( 'Custom Label','None',new List<String>{'French'} );
        
        System.assertEquals( 'Tester',labelsList[0].langWithTranslation.get('French') );

        String response = TranslationComponent_Ctlr.createTranslation( new List<String>{'French'},  JSON.serialize(labelsList), 'Custom Label', 'None' );
        
        System.assertEquals( 'Success', response );
        
        Test.stopTest();
        
    }

    @isTest
    private static void getTranslationOfFields(){
        
        Test.startTest();	
        
        Test.setMock( WebServiceMock.class, new TranslationComponentMock() );
        
        List<TranslationComponent_Ctlr.LabelWrapper> labelsList = TranslationComponent_Ctlr.getLabelAccordingToCategory( 'Custom Field','Account',new List<String>{'Bulgarian'} );
        
        System.debug('labelsList'+labelsList);
        
        System.assertEquals( 'Тествайте потребителското поле',labelsList[0].langWithTranslation.get('Bulgarian') );
        
        for ( TranslationComponent_Ctlr.LabelWrapper label : labelsList ){
            
            if( label.labelAPIName == 'Test_Custom_Field__c') 
                label.langWithTranslation.put( 'Japanese','' );
            
        }
        
        Test.setMock( HttpCalloutMock.class, new TranslationAPIMock() );
        
        List<TranslationComponent_Ctlr.LabelWrapper> result = TranslationComponent_Ctlr.translation( new List<String>{'Bulgarian','Japanese'}, JSON.serialize(labelsList) );
        
        System.assertEquals( 'カスタムフィールドをテスト',result[0].langWithTranslation.get('Japanese') );
        
        Test.stopTest();
        
    }
    
    @isTest
    private static void getRecordsForFieldSetComponent(){
        
        Test.startTest();	
        
        Test.setMock( WebServiceMock.class, new TranslationComponentMock() );
        
        List<TranslationComponent_Ctlr.LabelWrapper> labelsList = TranslationComponent_Ctlr.getLabelAccordingToCategory( 'Field Set','Account',new List<String>{'Bulgarian'});
        
        System.assertEquals( 'Тестово поле Задайте профил',labelsList[0].langWithTranslation.get('Bulgarian') );
        
        for ( TranslationComponent_Ctlr.LabelWrapper label : labelsList ){
            
            if( label.labelAPIName == 'Test Field Set Of Account')
                label.langWithTranslation.put( 'French','Jeu de compte de terrain de test' );
            
        }
        
        String response = TranslationComponent_Ctlr.createTranslation( new List<String>{'French'},  JSON.serialize(labelsList), 'Field Set', 'Account');
        
        System.assertEquals( 'Success', response );
        
        Test.stopTest();
        
    }
    
    @isTest
    private static void getRecordsForCustomFieldComponent(){
        
        Test.startTest();	
        
        Test.setMock( WebServiceMock.class, new TranslationComponentMock() );
        
        List<TranslationComponent_Ctlr.LabelWrapper> labelsList = TranslationComponent_Ctlr.getLabelAccordingToCategory( 'Custom Field','Account',new List<String>{'Bulgarian'});
        
        System.debug('labelsList'+labelsList);
        
        System.assertEquals( 'Тествайте потребителското поле',labelsList[0].langWithTranslation.get('Bulgarian'));
        
        for ( TranslationComponent_Ctlr.LabelWrapper label : labelsList ){
            
            if( label.labelAPIName == 'Test_Custom_Field__c') 
                label.langWithTranslation.put( 'French','Tester le champ personnalisé' );
            
        }
        
        String response = TranslationComponent_Ctlr.createTranslation( new List<String>{'French'},  JSON.serialize(labelsList), 'Custom Field', 'Account' );
        
        System.assertEquals( 'Success', response );
        
        Test.stopTest();
        
    }
    
    @isTest
    private static void getRecordsForWebLinkComponent(){
        
        Test.startTest();	
        
        Test.setMock( WebServiceMock.class, new TranslationComponentMock() );
        
        List<TranslationComponent_Ctlr.LabelWrapper> labelsList = TranslationComponent_Ctlr.getLabelAccordingToCategory( 'Button and Link Label','Account',new List<String>{'Bulgarian'} );
        
        System.assertEquals( 'Тестова връзка',labelsList[0].langWithTranslation.get('Bulgarian') );
        
        for ( TranslationComponent_Ctlr.LabelWrapper label : labelsList ){
            
            if( label.labelAPIName == 'Test_Link') 
                label.langWithTranslation.put( 'French','Lien de test' );
            
        }
        
        String response = TranslationComponent_Ctlr.createTranslation( new List<String>{'French'},  JSON.serialize(labelsList), 'Button and Link Label', 'Account' );
        
        System.assertEquals( 'Success', response );
        
        Test.stopTest();
        
    }
    
    @isTest
    private static void getRecordsForRecordTypeComponent(){
        
        Test.startTest();	
        
        Test.setMock( WebServiceMock.class, new TranslationComponentMock() );
        
        List<TranslationComponent_Ctlr.LabelWrapper> labelsList = TranslationComponent_Ctlr.getLabelAccordingToCategory( 'Record Type','Account',new List<String>{'Bulgarian'} );
        
		System.assertEquals( 'Тип тест запис',labelsList[0].langWithTranslation.get('Bulgarian') );
        
        for ( TranslationComponent_Ctlr.LabelWrapper label : labelsList ){
            
            if( label.labelAPIName == 'Test_Record_Type' ) 
                label.langWithTranslation.put( 'French','Type denregistrement de test' );
            
        }
        
        String response = TranslationComponent_Ctlr.createTranslation( new List<String>{'French'},  JSON.serialize(labelsList), 'Record Type', 'Account' );
        
        System.assertEquals( 'Success', response );
        
        Test.stopTest();
        
    }
}