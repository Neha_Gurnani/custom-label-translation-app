/* 
 * Name        : TranslationComponent_Ctlr
 * Created By  : Concret.io Apps
 * Description : Controller have methods that can be called from Lightning Component and translate into specific language and create translation metdata
*/

public class TranslationComponent_Ctlr{
    
    Private static final String CODE_OF_USER_LANGUAGE = UserInfo.getLanguage().left(2);
    Private static final String TRANSLATE_API = 'https://translate.yandex.net/api/v1.5/tr.json/translate?lang='+CODE_OF_USER_LANGUAGE+'-';
    Private static final String KEY = '&key=trnsl.1.1.20190604T125030Z.4d55ea8a69a73ac7.173cba7484bab192e266ba6fb64563b598d2d750';
    
    @AuraEnabled
    public static Map<String,String> getMetadataComponentsAndLanguages(){
        
        Map<String,String> langAndComponents = new Map<String,String>();
        
        Map<String,Metadata_Component__c>  componentsMap = Metadata_Component__c.getAll();
        
        Map<String, Languages__c> languageMap = getLanguages();
        
        if( !componentsMap.isEmpty() ){
            
            langAndComponents.put( 'Components',JSON.serialize( componentsMap.keySet() ) );
            
        }
        
        if( !languageMap.isEmpty() ){
            
            langAndComponents.put( 'Languages',JSON.serialize(languageMap.keySet()) );
            
        }
        
        return langAndComponents;
        
    }
    
    @AuraEnabled
    public static Map<String,String> getCategories( String selectedComponent ){
        
        Map<String,String> langAndLabelsMap = new Map<String,String>();
        
        if( selectedComponent == 'Custom Label' ){
            
            Map<String,List<LabelWrapper>> labels = getLabels(); 
            
            if( !labels.isEmpty() ){
                
                langAndLabelsMap.put( 'Category' , JSON.serialize( labels.keySet() ) );
                
            }
            
        }
        
        else if(selectedComponent == 'Custom Field' || selectedComponent == 'Record Type' || selectedComponent == 'Field Set' || selectedComponent == 'Button and Link Label' || selectedComponent == 'Workflow Task'){
            
            List<String> objectList = getObjects();
            
            if( !objectList.isEmpty() ){
                
                objectList.sort();
                langAndLabelsMap.put( 'Category' , JSON.serialize(objectList) );
                
            }
            
        }
        
        return langAndLabelsMap;
        
    }
    
    private static Map<String, Languages__c> getLanguages(){
        
        Map<String,Languages__c> languageMap = new Map<String,Languages__c>();
        
        languageMap = Languages__c.getAll();
        
        return languageMap;
        
    }
    
    private static Map<String,List<LabelWrapper>> getLabels(){
        
        List<MetadataService.Metadata> result = getCustomLabelsUsingMetadata();
        
        MetadataService.CustomLabel label = new MetadataService.CustomLabel();
        
        System.debug('result'+result);
        
        Map<String,List<LabelWrapper>> mapOfLabels = new Map<String,List<LabelWrapper>>();
        
        for( MetadataService.Metadata res : result ){
            
            List<LabelWrapper> labelNames = new List<LabelWrapper>();
            
            label = ( MetadataService.CustomLabel ) res;
            
            String category;
            
            if( label.categories == null ){
                
                category = 'None';
                
            }
            
            else{
                
                category = label.categories;
                
            }
            
            if( mapOfLabels.containsKey( category ) ){
                
                labelNames = mapOfLabels.get( category );
                labelNames.add( new LabelWrapper( label.fullName,label.value,label.fullName ) );
                
            }
            
            else{
                
                labelNames.add( new LabelWrapper( label.fullName,label.value,label.fullName ) );
                
            } 
            
            mapOfLabels.put( category, labelNames );
            
        }
        
        System.debug( 'MapOfLabels '+mapOfLabels );
        
        return mapOfLabels;
        
    }
    
    private static List<MetadataService.Metadata> getCustomLabelsUsingMetadata(){
        
        MetadataService.ListMetadataQuery query = new MetadataService.ListMetadataQuery();
        query.type_x = 'CustomLabel';
        Double asOfVersion = 45.0;
        
        MetadataService.FileProperties[] files = service.listMetadata( new MetadataService.ListMetadataQuery[]{query}, 
                                                                      asOfVersion );
        
        System.debug('files :: '+files);
        
        List<MetadataService.Metadata> customLabelResult = new List<MetadataService.Metadata>();
        
        List<String> customLabelList = new List<String>();
        
        if( files != null && !files.isEmpty() ){
            
            for(Integer i = 0; i < files.size(); i++){
                
                if( files[i].namespacePrefix == null ){
                    
                    customLabelList.add( files[i].fullName );
                    
                }
                
                System.debug('customLabelList'+customLabelList);
                
                if( customLabelList.size() == 10 || i == files.size()-1 ){
                    
                    if( !customLabelList .isEmpty() ){
                        
                        MetadataService.IReadResult result = service.readMetadata( 'CustomLabel',customLabelList );
                        
                        customLabelResult.addAll( result.getRecords() );
                        
                        customLabelList.clear();
                        
                    }
                    
                }
            }
        }
        
        return customLabelResult;
        
    }
    
    private static List<String> getObjects(){
        
        List<String> objectNames = new List<String>();
        
        Map<String, Schema.SObjectType> objectMap = Schema.getGlobalDescribe();
        
        for( Schema.SObjectType obj : objectMap.values() ){
            
            Schema.DescribeSObjectResult objResult = obj.getDescribe();
            
            if(objResult.isAccessible() && objResult.isQueryable() && objResult.isSearchable()){
                
                objectNames.add(objResult.getLabel());
                
            }
        }
        
        return objectNames;
        
    }    
    
    @AuraEnabled
    public static List<LabelWrapper> getLabelAccordingToCategory( String selectedComponent,String category,List<String> selectedLanguages ){
        
        List<LabelWrapper> listOfRecords = new List<LabelWrapper>();
        
        if( selectedComponent == 'Custom Label'){
            
            Map<String,List<LabelWrapper>> labels = getLabels();
            
            if( !labels.isEmpty() ){
                
                if( category != null ){
                    
                    listOfRecords.addAll( labels.get(category) );
                    
                }
                
                System.debug('selectedLanguages'+selectedLanguages);
                
                if( !selectedLanguages.isEmpty() && !listOfRecords.isEmpty()){
                    
                    getTranslations( selectedLanguages,listOfRecords );
                    
                }
                
            }
            
        }
        
        else if(selectedComponent == 'Custom Field' || selectedComponent == 'Record Type' 
                || selectedComponent == 'Field Set' || selectedComponent == 'Button and Link Label' 
                || selectedComponent == 'Workflow Task'){
                    
                    listOfRecords = getObjectsDetails( selectedComponent, category);
                    
                    System.debug('Size'+listOfRecords.size());
                    
                    if( !selectedLanguages.isEmpty() && !listOfRecords.isEmpty()){
                        
                        getObjectTranslation( selectedComponent, listOfRecords, category, selectedLanguages );
                        
                    }
                    
                } 
        
        
        return listOfRecords;
        
    }
    
    private static List<LabelWrapper> getObjectsDetails( String selectedComponent, String objectName ){
        
        List <String> objectNames = new List <String>();
        
        objectNames.add( objectName );
        
        System.debug('objectNames '+objectNames);
        
        List<LabelWrapper> resultList = new List<LabelWrapper>();
        
        MetadataService.IReadResult readResult = service.readMetadata( 'CustomObject', objectNames );
        
        MetadataService.Metadata[] metadataInfo = readResult.getRecords();
        
        List<MetadataService.CustomObject> objectList = new List<MetadataService.CustomObject>();
        
        System.debug( 'Number of component info returned: ' + metadataInfo );
        
        for (MetadataService.Metadata metadata : metadataInfo) {
            
            if ( metadata != null ){
                
                MetadataService.CustomObject obj = (MetadataService.CustomObject) metadata;
                
                System.debug('Custom object full name: '+ obj);
                
                objectList.add(obj);
                
            }
            
        }
        
        resultList = getSelectedComponentWrapperList( objectList, selectedComponent );
        
        return resultList;
    }
    
    private static List<LabelWrapper> getSelectedComponentWrapperList( List<MetadataService.CustomObject> objects,String selectedComponent ){
        
        List <LabelWrapper> wrapperList = new List <LabelWrapper>();
        
        //Map<String,List<MetadataService.WorkflowTask>> workflowTasksMap = new Map<String,List<MetadataService.WorkflowTask>>();   
        
        /*if( selectedComponent == 'Workflow Task'){
            workflowTasksMap = getWorkflowTask(objects);
        }*/
        
        for( MetadataService.CustomObject obj : objects){
            
            if( selectedComponent == 'Custom Field' ){
                
                if( obj.fields != null ){
                    
                    for( MetadataService.CustomField field : obj.fields ) {
                        
                        if( field != null ) {
                            
                            if(field.fullName.endsWithIgnoreCase( '__c' )){
                                
                                wrapperList.add( new LabelWrapper( field.label,field.label,field.fullName ));
                                
                            }                       
                        }
                    }
                }
            }
            
            else if( selectedComponent == 'Field Set' ){
                
                if( obj.fieldSets != null ){
                    
                    for( MetadataService.FieldSet fieldSet : obj.fieldSets ){
                        
                        if( fieldSet != null ) {
                            
                            wrapperList.add( new LabelWrapper( fieldSet.label,fieldSet.label,fieldSet.fullName ));
                            
                        }
                    }
                }
            }
            
            else if( selectedComponent == 'Button and Link Label' ){
                
                if( obj.webLinks != null ){
                    
                    for( MetadataService.WebLink btnAndLink : obj.webLinks ) {
                        
                        if( btnAndLink != null ) {
                            
                            wrapperList.add( new LabelWrapper( btnAndLink.masterLabel,btnAndLink.masterLabel,btnAndLink.fullName ));
                            
                        }
                    }
                }
            }
            
            else if( selectedComponent == 'Record Type' ){
                
                if( obj.recordTypes != null ){
                    
                    for( MetadataService.RecordType recType : obj.recordTypes ){
                        
                        if( recType != null ) {
                            
                            wrapperList.add( new LabelWrapper( recType.label,recType.label,recType.fullName ));
                            
                        }
                    }
                }
            }
            
            /*else if( selectedComponent == 'Workflow Task'){
                
                if( workflowTasksMap != null){
                    
                    for( MetadataService.WorkflowTask task : workflowTasksMap.get( obj.fullName )){

                        wrapperList.add(new LabelWrapper( task.subject,task.subject,task.fullName ));

                    }
                }
            }*/
        }
        
        return wrapperList;
        
    }
    
    private static void getTranslations( List<String> selectedLanguage, List<LabelWrapper> labels ){
        
        Map<String,LabelWrapper> labelMap = new Map<String,LabelWrapper>();
        
        for( LabelWrapper label : labels ){
            
            labelMap.put( label.labelAPIName,label );
            
        }
        
        List<String> translationList = new List<String>();
        Map<String,String> translationLanguageMap = new Map<String,String>();
        
        Map<String, Languages__c> languageMap = Languages__c.getAll();
        
        for(String language : selectedLanguage){
            
            String code = languageMap.get( language ).Locale_Code__c;
            
            translationList.add( code );
            
            translationLanguageMap.put( code,language );
            
        }
        
        MetadataService.IReadResult result = service.readMetadata( 'Translations',translationList );
        
        Map<String,List<MetaDataService.CustomLabelTranslation>> translationMapOfCustomLabel = new Map<String,List<MetaDataService.CustomLabelTranslation>>();
        
        for( MetadataService.Metadata rec : result.getRecords() ){
            
            MetadataService.Translations res = ( MetadataService.Translations )rec;
            
            if( res.customLabels != null){
                
                translationMapOfCustomLabel.put( res.fullName,res.customLabels );
            }
            
        }
        
        for( String translatedLang : translationMapOfCustomLabel.keySet() ){
            
            List<MetaDataService.CustomLabelTranslation> translationOfCustomLabel =  translationMapOfCustomLabel.get(translatedLang);
            
            String language = translationLanguageMap.get( translatedLang );
            
            Map<String,String> data = new Map<String,String>();
            
            if( !translationOfCustomLabel.isEmpty() ){
                
                for( MetaDataService.CustomLabelTranslation translation : translationOfCustomLabel ){
                    
                    if( labelMap.get( translation.name ) != null){
                        
                        labelMap.get( translation.name ).langWithTranslation.put( language,translation.Label );
                        
                    }
                }
            }
        }
    }
    
    private static void getObjectTranslation( String selectedComponent, List<LabelWrapper> records, String objectName,List<String> selectedLanguages ){
        
        Map<String, Languages__c> languageMap = Languages__c.getAll();
        
        Map<String,LabelWrapper> recordMap = new Map<String,LabelWrapper>();
        List<String> customObjectList = new List<String>();
        Map<String,String> translatonLanguageMap = new Map<String,String>();
        
        for( LabelWrapper field : records ){
            
            recordMap.put(field.labelAPIName, field);
            
        }
        
        for( String language : selectedLanguages ) {
            
            customObjectList.add( objectName + '-' + languageMap.get(language).Locale_Code__c );
            translatonLanguageMap.put( objectName + '-' + languageMap.get(language).Locale_Code__c,language );
            
        }
        
        System.debug('customObjectList'+customObjectList);
        System.debug('translatonLanguageMap'+translatonLanguageMap);
        
        MetadataService.IReadResult result = service.readMetadata('CustomObjectTranslation',customObjectList );
        
        List<MetadataService.CustomObjectTranslation> customObjectRecords = new List<MetadataService.CustomObjectTranslation>();
        
        for(MetadataService.Metadata rec : result.getRecords()){
            
            MetadataService.CustomObjectTranslation res = (MetadataService.CustomObjectTranslation)rec;
            customObjectRecords.add(res); 
            
        }
        Map<String,Map<String,String>> resultTranslationMap = new Map<String,Map<String,String>>();
        
        if( selectedComponent == 'Custom Field' ){
            
            getCustomFieldsTranslation( customObjectRecords,recordMap,translatonLanguageMap );
            
        }
        
        else if( selectedComponent == 'Field Set' ){
            
            getFieldSetTranslation( customObjectRecords,recordMap,translatonLanguageMap );
            
        }
        
        else if( selectedComponent == 'Button and Link Label' ){
            
            getWebLinkTranslation( customObjectRecords,recordMap,translatonLanguageMap );
            
        }
        
        else if( selectedComponent == 'Record Type' ){
            
            getRecordTypeTranslation( customObjectRecords,recordMap,translatonLanguageMap );
            
        }
        
        /*else if( selectedComponent == 'Workflow Task'){
            
            getWorkflowTaskTranslation( customObjectRecords,recordMap,translatonLanguageMap );
            
        }*/
        
    }
    
    private static void getCustomFieldsTranslation( List<MetadataService.CustomObjectTranslation> customObjects,Map<String,LabelWrapper> recordMap,Map<String,String> translatonLanguageMap ){
        
        Map<String,List<MetadataService.CustomFieldTranslation>> translationMap = new  Map<String,List<MetadataService.CustomFieldTranslation>>();
        
        for(MetadataService.CustomObjectTranslation obj : customObjects ){
            
            translationMap.put( obj.fullName,obj.fields );
            
        }
        
        System.debug('translationMap'+translationMap);
        
        for( String key : translationMap.keySet() ){
            
            String language = translatonLanguageMap.get(key);
            
            for(MetadataService.CustomFieldTranslation fieldTranslation : translationMap.get(key)){
                
                if( recordMap.get( fieldTranslation.name) != null){
                    
                    recordMap.get( fieldTranslation.name).langwithTranslation.put( language,fieldTranslation.label );
                    
                }
                
            }
            
        }
        
    }
    
    private static void getFieldSetTranslation( List<MetadataService.CustomObjectTranslation> customObjects,Map<String,LabelWrapper> recordMap,Map<String,String> translatonLanguageMap ){
        
        Map<String,List<MetadataService.FieldSetTranslation>> translationMap = new  Map<String,List<MetadataService.FieldSetTranslation>>();
        
        for(MetadataService.CustomObjectTranslation obj : customObjects ){
            
            translationMap.put( obj.fullName,obj.fieldSets );
        }
        
        System.debug('translationMap :: '+translationMap);
        
        for( String key : translationMap.keySet()){
            
            for(MetadataService.FieldSetTranslation fieldSetTranslation : translationMap.get(key)){
                
                String language = translatonLanguageMap.get(key);
                
                if( recordMap.get( fieldSetTranslation.name) != null){
                    
                    recordMap.get( fieldSetTranslation.name).langwithTranslation.put( language,fieldSetTranslation.label );
                    
                }
                
            }
            
        }
        
    }
    
    private static void getWebLinkTranslation( List<MetadataService.CustomObjectTranslation> customObjects,Map<String,LabelWrapper> recordMap,Map<String,String> translatonLanguageMap ){
        
        Map<String,List<MetadataService.WebLinkTranslation>> translationMap = new  Map<String,List<MetadataService.WebLinkTranslation>>();
        
        for(MetadataService.CustomObjectTranslation obj : customObjects ){
            
            translationMap.put( obj.fullName,obj.webLinks );
            
        }
        
        for( String key : translationMap.keySet() ){
            
            String language = translatonLanguageMap.get( key );
            
            for( MetadataService.WebLinkTranslation webLinkTranslation : translationMap.get(key) ){
                
                if( recordMap.get( webLinkTranslation.name ) != null){
                    
                    recordMap.get( webLinkTranslation.name ).langwithTranslation.put( language,webLinkTranslation.label );
                    
                }
                
            }
            
        }
        
    }
    
    private static void getRecordTypeTranslation( List<MetadataService.CustomObjectTranslation> customObjects,Map<String,LabelWrapper> recordMap,Map<String,String> translatonLanguageMap ){
        
        Map<String,List<MetadataService.RecordTypeTranslation>> translationMap = new  Map<String,List<MetadataService.RecordTypeTranslation>>();
        
        for( MetadataService.CustomObjectTranslation obj : customObjects ){
            
            translationMap.put( obj.fullName,obj.recordTypes );
            
        }
        
        System.debug('translationMap :: '+translationMap);
        
        for( String key : translationMap.keySet() ){
            
            String language = translatonLanguageMap.get( key );
            
            for( MetadataService.RecordTypeTranslation recordTypeTranslation : translationMap.get( key )){
                
                if( recordMap.get( recordTypeTranslation.name ) != null){
                    
                    recordMap.get( recordTypeTranslation.name ).langwithTranslation.put( language,recordTypeTranslation.label );
                    
                }
                
            }
            
        }
        
    }
    
    /*private static void getWorkflowTaskTranslation( List<MetadataService.CustomObjectTranslation> customObjects,Map<String,LabelWrapper> recordMap,Map<String,String> translatonLanguageMap ){
        
        Map<String,List<MetadataService.WorkflowTaskTranslation>> translationMap = new  Map<String,List<MetadataService.WorkflowTaskTranslation>>();
        
        for(MetadataService.CustomObjectTranslation obj : customObjects ){
            
            translationMap.put( obj.fullName,obj.workflowTasks );
            
        }
        
        for( String key : translationMap.keySet() ){
            
            String language = translatonLanguageMap.get( key );
            
            for( MetadataService.WorkflowTaskTranslation workflowTaskTranslation : translationMap.get(key) ){
                
                if( recordMap.get( workflowTaskTranslation.name ) != null){
                    
                    recordMap.get( workflowTaskTranslation.name ).langwithTranslation.put( language,workflowTaskTranslation.subject );
                    
                }
                
            }
            
        }
        
    }*/
    
    @AuraEnabled
    public static List<LabelWrapper> translation( List<String> languages, String labelJSON ){
        
        Map<String,Languages__c> languageMap = getLanguages();
        
        String languageCode = null;
        
        List<LabelWrapper> Labels = new List<LabelWrapper>();
        
        if( labelJSON != null ){
            
            Labels = ( List<LabelWrapper> )JSON.deserialize( LabelJSON, List<LabelWrapper>.class );
            
        }
        
        String textToConvert = '';
        
        Map<String,List<LabelWrapper>> labelWrapperWithLang = new  Map<String,List<LabelWrapper>>();
        
        for( String language : languages ){
            
            for( LabelWrapper label : Labels ){
                
                String value = label.langWithTranslation.get( language );
                
                if( value == '' || value == null ){
                    
                    if( labelWrapperWithLang.containsKey(language) ){
                        
                        labelWrapperWithLang.get( language ).add( label );
                        
                    }
                    else{
                        
                        List<LabelWrapper> rec = new List<LabelWrapper>();
                        rec.add( label );
                        labelWrapperWithLang.put( language,rec );
                        
                    }
                }
            }
        }
        
        for( String lang : labelWrapperWithLang.keySet() ){
            
            if( !languageMap.isEmpty() ){
                
                languageCode = languageMap.get( lang ).Language_Code__c;
                
            }
            for( LabelWrapper label : labelWrapperWithLang.get(lang) ){
                
                String val = label.value;
                val = EncodingUtil.URLENCODE(val,'UTF-8');
                textToConvert = textToConvert+'&text='+val;
                
            }
            
            
            String url =  TRANSLATE_API + languageCode + KEY + textToConvert;
            
            System.debug('url :: '+url);
            
            HttpResponse response = request( url );
            
            System.debug('response '+response.getStatusCode());
            
            System.debug('h  :: '+response.getBody());
            
            if( response.getStatusCode() == 200 ){
                
                TranslationWrapper translationResult = (TranslationWrapper)JSON.deserialize( response.getBody(),TranslationWrapper.class );
                
                System.debug('t :: '+translationResult);
                
                List<String> listLabels = translationResult.text;
                
                System.debug('listLabels :: '+listLabels);
                
                for(LabelWrapper lab : labelWrapperWithLang.get(lang)){
                    
                    lab.langWithTranslation.put( lang, listLabels[labelWrapperWithLang.get(lang).indexOf(lab)] );
                }
            }
        }
        
        for(String lan : languages){
            
            for(LabelWrapper rec : Labels){
                
                String value = rec.langWithTranslation.get(lan);
                
            }
        }
        
        return labels;
        
    }
    
    private static HttpResponse request( String url ){
        
        Http h=new Http();
        
        HttpRequest request=new HttpRequest();
        
        request.setEndPoint(url);
        
        request.setMethod('GET');
        
        return h.send(request);
    }
    
    private static MetaDataService.MetadataPort service{
        
        get
        {
            
            if(service == NULL)
            {
                
                service = new MetadataService.MetadataPort();
                
                service.SessionHeader = new MetadataService.SessionHeader_element();
                
                // service.SessionHeader.sessionId = UserInfo.getSessionId();
                service.SessionHeader.sessionId = getSessionId();
                
            } 
            return service;
            
        }set;
        
    }
    
    private static String getSessionId(){
        
        if( Test.isRunningTest() ){
            
            String sessionId =UserInfo.getSessionId();
            return sessionId;
            
        }
        
        PageReference reportPage = Page.SessionIdvfPage;
        System.debug('report '+reportPage);
        
        // Get the content of the VF page
        String vfContent = reportPage.getContent().toString();
        System.debug('vfContent '+vfContent);
        
        // Find the position of Start_Of_Session_Id and End_Of_Session_Id
        Integer startP = vfContent.indexOf('Start_Of_Session_Id') + 'Start_Of_Session_Id'.length(),
            endP = vfContent.indexOf('End_Of_Session_Id');
        
        // Get the Session Id
        //  String sessionId =UserInfo.getSessionId();
        String sessionId = vfContent.substring(startP, endP);
        
        return sessionId;
        
    }
    
    public class TranslationWrapper{
        
        public Integer code;	//200
        public String lang;
        public String[] text;
        
    }
    
    public class LabelWrapper{
        
        @AuraEnabled public String label;
        @AuraEnabled public String value;
        @AuraEnabled public String labelAPIName;
        @AuraEnabled public Map<String,String> langWithTranslation = new Map<String,String>();
        
        
        public LabelWrapper( String labelFullName,String val, String labelAPI ){
            
            label = labelFullName;
            value = val;
            labelAPIName = labelAPI;
            
        }
        
    }
    
    @AuraEnabled
    public static String createTranslation( List<String> selectedLanguages,String labelJSON,String selectedComponent, String category ){
        
        System.debug('selectedLanguages'+selectedLanguages);
        
        List<LabelWrapper> labels = new List<LabelWrapper>();
        
        String result = '';
        
        if( labelJSON != null ){
            
            labels = (List<LabelWrapper>)JSON.deserialize(labelJSON, List<LabelWrapper>.class);
        }
        
        System.debug('labels :: '+labels);
        System.debug('labelMap :: '+labelJSON);
        
        Map<String, Languages__c> languageMap = Languages__c.getAll();
        
        if( selectedComponent == 'Custom Label'){
            
            result = createCustomLabelTranslation( selectedLanguages,labels );
            
        }
        else{
            
            result = createCustomObjectTranslation( selectedLanguages,labels,selectedComponent,category );
        }
        
        return result;
        
    }
    
    private static String createCustomLabelTranslation( List<String> selectedLanguages,List<LabelWrapper> labels ){
        
        Map<String, Languages__c> languageMap = Languages__c.getAll();
        
        for( String language : selectedLanguages ){
            
            String fileName = languageMap.get(language).Locale_Code__c;
            
            MetadataService.Translations translations = new MetadataService.Translations();
            List<MetaDataService.CustomLabelTranslation> customLabel = new List<MetaDataService.CustomLabelTranslation>();
            
            for( LabelWrapper label : labels ){
                
                MetaDataService.CustomLabelTranslation cusLabel = new MetaDataService.CustomLabelTranslation();
                
                cusLabel.name = label.labelAPIName;
                cusLabel.label = label.langWithTranslation.get( language );
                customLabel.add( cusLabel );
                
            }
            
            translations.fullName = fileName;
            translations.customLabels = customLabel;
            MetadataService.SaveResult[] results = service.createMetadata( new List<MetadataService.Metadata>{ translations } );
            
        }
        
        return 'Success';
        
    }
    
    private static String createCustomObjectTranslation( List<String> selectedLanguages,List<LabelWrapper> labels, String selectedComponent,String category ){
        
        Map<String, Languages__c> languageMap = Languages__c.getAll();
        
        for(String language : selectedLanguages){
            
            String fileName = category + '-' +languageMap.get(language).Locale_Code__c;
            
            MetadataService.CustomObjectTranslation cusObjTranslations = new MetadataService.CustomObjectTranslation();
            
            List<MetadataService.CustomFieldTranslation> customFields = new List<MetadataService.CustomFieldTranslation>();
            List<MetadataService.FieldSetTranslation> fieldSets = new List<MetadataService.FieldSetTranslation>();
            List<MetadataService.RecordTypeTranslation> recordTypes = new List<MetadataService.RecordTypeTranslation>();
            List<MetadataService.WebLinkTranslation> webLinks = new List<MetadataService.WebLinkTranslation>();
            List<MetadataService.WorkflowTaskTranslation> workflowTasks = new List<MetadataService.WorkflowTaskTranslation>();
            
            for( LabelWrapper label : labels ){
                
                if( selectedComponent == 'Custom Field' ){
                    
                    MetadataService.CustomFieldTranslation field = new MetadataService.CustomFieldTranslation();
                    field.name = label.labelAPIName;
                    field.label = label.langWithTranslation.get(language);
                    customFields.add(field);
                    
                }
                
                else if( selectedComponent == 'Field Set' ){
                    
                    MetadataService.FieldSetTranslation fieldSet = new MetadataService.FieldSetTranslation();
                    fieldSet.name = label.labelAPIName;
                    fieldSet.label = label.langWithTranslation.get(language);
                    fieldSets.add(fieldSet);
                    
                }
                
                else if( selectedComponent == 'Button and Link Label' ){
                    
                    MetadataService.WebLinkTranslation webLink = new MetadataService.WebLinkTranslation();
                    webLink.name = label.labelAPIName;
                    webLink.label = label.langWithTranslation.get(language);
                    webLinks.add(webLink);
                    
                }
                
                else if( selectedComponent == 'Record Type' ){
                    
                    MetadataService.RecordTypeTranslation recType = new MetadataService.RecordTypeTranslation();
                    recType.name = label.labelAPIName;
                    recType.label = label.langWithTranslation.get(language);
                    recordTypes.add(recType);
                    
                }
                
                /*else if( selectedComponent == 'Workflow Task' ){
                    
                    MetadataService.WorkflowTaskTranslation task = new MetadataService.WorkflowTaskTranslation();
                    task.name = label.labelAPIName;
                    task.subject = label.langWithTranslation.get(language);
                    workflowTasks.add(task);
                    
                }*/
                
            }
            
            cusObjTranslations.fullName = fileName;
            cusObjTranslations.fields = customFields;
            cusObjTranslations.fieldSets = fieldSets;
            cusObjTranslations.recordTypes = recordTypes;
            cusObjTranslations.webLinks = webLinks;
            cusObjTranslations.workflowTasks = workflowTasks;
            
            MetadataService.SaveResult[] results = service.createMetadata( new List<MetadataService.Metadata>{cusObjTranslations}  );
            
            System.debug('result '+results);
            
        }
        
        return 'Success';
        
    }
    
    
    /*private static Map<String,List<MetadataService.WorkflowTask>> getWorkflowTask( List<MetadataService.CustomObject> objects ){
        
        List<String> objectFullNameList = new List<String>();
        Map<String,List<MetadataService.WorkflowTask>> workflowTasksMap = new Map<String,List<MetadataService.WorkflowTask>>();
        
        for( MetadataService.CustomObject obj : objects ){
            
            objectFullNameList.add( obj.fullName );
            
        }
        
        List<MetadataService.Metadata> metadataInfo = (List<MetadataService.Metadata>)service.readMetadata( 'Workflow', objectFullNameList ).getRecords();
        
        for( MetadataService.Metadata metadata : metadataInfo ){
            
            MetadataService.Workflow workflow = (MetadataService.Workflow) metadata ;
            
            if ( !workflow.tasks.isEmpty() ){
                
                workflowTasksMap.put( workflow.fullName, workflow.tasks);
                
            }
        }
        
        return workflowTasksMap;
        
    }*/
    
}